#pragma once
struct Color {
	float r = 0.0f;
	float g = 0.0f;
	float b = 0.0f;
} ;


inline Color operator * (const Color &c1, const Color &c2) {
	Color newColor;
	newColor.r = c1.r * c2.r;
	newColor.g = c1.g * c2.g;
	newColor.b = c1.b * c2.b;
	return newColor;
}


inline Color operator / (const Color &c1, const Color &c2) {
	Color newColor;
	newColor.r = c1.r / c2.r;
	newColor.g = c1.g / c2.g;
	newColor.b = c1.b / c2.b;
	return newColor;
}


inline Color operator * (const Color &c, const float scalar) {
	Color newColor;
	newColor.r = c.r * scalar;
	newColor.g = c.g * scalar;
	newColor.b = c.b * scalar;
	return newColor;
}


inline Color operator / (const Color &c, const float scalar) {
	Color newColor;
	newColor.r = c.r / scalar;
	newColor.g = c.g / scalar;
	newColor.b = c.b / scalar;
	return newColor;
}


