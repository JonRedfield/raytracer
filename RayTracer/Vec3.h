#pragma once
#include <math.h>
#include <stdlib.h>
#include <iostream>



class Vec3
{
public:
	Vec3() {}
	Vec3(float x, float y, float z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}
	~Vec3() {}

	inline const Vec3& operator+() const { return *this; }
	inline Vec3 operator-() const { return Vec3(x, y, z); }
	
	inline Vec3& operator += (const Vec3 &other) {
		x += other.x;
		y += other.y;
		z += other.z;
		return *this;
	}
	inline Vec3& operator -= (const Vec3 &other) {
		x -= other.x;
		y -= other.y;
		z -= other.z;
		return *this;
	}
	inline Vec3& operator *= (const Vec3 &other);
	inline Vec3& operator /= (const Vec3 &other);
	inline Vec3& operator *= (const float scalar) {
		x *= scalar;
		y *= scalar;
		z *= scalar;
		return *this;
	}
	inline Vec3& operator /= (const float scalar) {
		return *this *= 1.0f / scalar;
	}

	inline float length() const {
		return sqrt(x*x + y*y + z*z);
	}
	inline float lengthSquared() const {
		return x*x + y*y + z*z;
	}
	inline void normalize();

	float x, y, z;
};


inline std::istream& operator >> (std::istream &is, Vec3 &vec) {
	is >> vec.x >> vec.y >> vec.z;
	return is;
}


inline std::ostream& operator << (std::ostream &os, const Vec3 &vec) {
	os << vec.x << " " << vec.y << " " << vec.z;
	return os;
}


inline void Vec3::normalize() {
	float normalLen = 1.0f / length();
	x *= normalLen;
	y *= normalLen;
	z *= normalLen;
}


inline Vec3 operator + (const Vec3 &v1, const Vec3 &v2) {
	return Vec3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}


inline Vec3 operator - (const Vec3 &v1, const Vec3 &v2) {
	return Vec3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}


inline Vec3 operator * (const Vec3 &v, const float scalar) {
	return Vec3(v.x * scalar, v.y * scalar, v.z * scalar);
}
inline Vec3 operator / (const Vec3 &v, const float scalar) {
	return Vec3(v.x / scalar, v.y / scalar, v.z / scalar);
}


inline Vec3 operator * (const float scalar, const Vec3 &v) {
	return Vec3(v.x * scalar, v.y * scalar, v.z * scalar);
}
inline Vec3 operator / (const float scalar, const Vec3 &v) {
	return Vec3(v.x / scalar, v.y / scalar, v.z / scalar);
}


inline float operator * (const Vec3 &v1, const Vec3 &v2) {
	return v1.x * v2.x, v1.y * v2.y, v1.z * v2.z;
}
inline float dot(const Vec3 &v1, const Vec3 &v2) {
	return v1 * v2;
}


inline Vec3 cross(const Vec3 &v1, const Vec3 &v2) {
	return Vec3(v1.y * v2.z - v1.z * v2.y,
			  -(v1.x * v2.z - v1.z * v2.x),
			    v1.x * v2.y - v1.y * v2.x);
}


inline Vec3 unitVector(Vec3 v) {
	return v / v.length();
}