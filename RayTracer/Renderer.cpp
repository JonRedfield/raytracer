#include "stdafx.h"
#include "Renderer.h"


Renderer::Renderer(const int width, const int height)
{
	this->width = width;
	this->height = height;
	init();
}


Renderer::~Renderer()
{
	delete[] this->canvas;
	this->canvas = NULL;
}



void Renderer::DrawTestImage() {
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			float u = float(x) / float(width);
			float v = float(y) / float(height);

			castRay(u, v);

			Color color;
			color.r = 255.99 * r;
			color.g = 255.99 * g;
			color.b = 255.99 * b;

			DrawPixel(x, y, color);
		}
	}
}


void Renderer::init() {
	canvas = new Color[width * height];
}


Color Renderer::castRay(const float u, const float v) {
	Ray ray(Vec3(0, 0, 0), )
	Color color; 
	//Vec3 normal = unitVector()
	return color;
}


void Renderer::DrawPixel(const int x, const int y, const Color color) {
	canvas[(width * y) + x] = color;
}


Color const Renderer::GetPixel(const int x, const int y) {
	return canvas[(width * y) + x];
}