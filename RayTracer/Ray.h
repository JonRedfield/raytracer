#pragma once
#include "Vec3.h"


class Ray
{
public:
	Ray();
	Ray(const Vec3& origin, const Vec3& direction) {
		this->origin = origin;
		this->direction = direction;
	}
	~Ray();

	Vec3 Origin() const { return origin; }
	Vec3 Direction() const { return direction; }
	Vec3 PointAtParameter(float t) const { return origin + t * direction; }

private:
	Vec3 origin;
	Vec3 direction;
};

