#pragma once
#include "Color.h"
#include "Ray.h"

class Renderer
{
private:
	int width;
	int height;
	Color* canvas = NULL;

	void init();
	Color castRay(const float u, const float v);


public:
	Renderer(const int width, const int height);
	~Renderer();
	void DrawTestImage();
	void DrawPixel(const int x, const int y, const Color color);
	Color const GetPixel(const int x, const int y);
};

